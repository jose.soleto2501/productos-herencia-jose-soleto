package productosJoseSoleto;

import java.util.ArrayList;
import java.util.Scanner;

public class Supermercado {
    private static ArrayList<Producto> productos;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion;
        do {
            mostrarMenu();
            while (!sc.hasNextInt()) {
                mostrarMenu();
                System.out.println("Selecciona una opción");
                sc.next();
            }
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    mostrarProductosFrescos();
                    break;
                case 2:
                    mostrarProductosRefrigerados();
                    break;
                case 3:
                    mostrarProductosCongelados();
                    break;

                default:
                    System.out.println("Elige entre una de las opciones");
            }
        } while (opcion != 0);

    }

    // Métodos de la clase Supermercado
    private static void mostrarProductosFrescos() {
        ProductoFresco prodFred1 = new ProductoFresco("11-11-2024", 1343, "11-11-2020", "España");
        ProductoFresco prodFred2 = new ProductoFresco("12-12-2024", 4321, "12-12-2020", "Italia");
    }

    private static void mostrarProductosRefrigerados() {
        System.out.println("Mostranod los productos refrigerados..");
    }

    private static void mostrarProductosCongelados() {
        System.out.println("Mostrando los productos congelados...");
    }

    private static void mostrarMenu() {
        System.out.println("|---Bienvenidos a la SIRENA!-----------|");
        System.out.println("|1.- Mostrar los productos frescos     |");
        System.out.println("|2.- Mostrar los productos refrigerados|");
        System.out.println("|3.- Mostrar los productos congelados  |");
        System.out.println("|0.- Salir                             |");
        System.out.println("|--------------------------------------|");
    }
}
