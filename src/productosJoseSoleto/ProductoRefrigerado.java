package productosJoseSoleto;

public class ProductoRefrigerado extends Producto {
    //Atributos de la clase ProductoRefrigerado
    private String codigoOrganismo;

    //Constructor de la clase ProductoRefrigerado
    public ProductoRefrigerado(String fechaCaducidad, int numeroLote, String codigoOrganismo) {
        super(fechaCaducidad, numeroLote);
        setCodigoOrganismo(codigoOrganismo);
    }
    //Getters y setters de la clase ProducctoRefrigerado
    public String getCodigoOrganismo() {
        return codigoOrganismo;
    }

    public void setCodigoOrganismo(String codigoOrganismo) {
        this.codigoOrganismo = codigoOrganismo;
    }

}
