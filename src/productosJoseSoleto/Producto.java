package productosJoseSoleto;

import java.util.ArrayList;

public class Producto {
    //Atributos de la clase Producto
    private String fechaCaducidad;
    private int numeroLote;
    private ArrayList<ProductoFresco> productosFrescos;
    private ArrayList<ProductoRefrigerado> productosRefrigerados;
    private ArrayList<ProductoCongelado> productosCongelados;
    

    //Constructor de la clase Producto    
    public Producto(String fechaCaducidad, int numeroLote) {
        setFechaCaducidad(fechaCaducidad);
        setNumeroLote(numeroLote);
    }
    //GETTERS Y SETTERS DE LA CLASE PRODUCTO
    public String getFechaCaducidad() {
        return fechaCaducidad;
    }
    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }
    public int getNumeroLote() {
        return numeroLote;
    }
    public void setNumeroLote(int numeroLote) {
        this.numeroLote = numeroLote;
    } 
}
