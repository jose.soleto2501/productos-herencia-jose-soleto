package productosJoseSoleto;

public class ProductoCongelado extends Producto {
    //Atributos de la clase ProductoCongelado
    private double temperaturaCongRecomendada;
    //Constructor de la clase ProductoCongelado
    public ProductoCongelado(String fechaCaducidad, int numeroLote, double temperaturaCongRecomendada) {
        super(fechaCaducidad, numeroLote);
        setTemperaturaCongRecomendada(temperaturaCongRecomendada);
    }
    
    //Getters y setters de la clase ProductoCongelado
    public double getTemperaturaCongRecomendada() {
        return temperaturaCongRecomendada;
    }

    public void setTemperaturaCongRecomendada(double temperaturaCongRecomendada) {
        this.temperaturaCongRecomendada = temperaturaCongRecomendada;
    }




}
